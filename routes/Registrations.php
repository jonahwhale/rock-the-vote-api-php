<?php
/**
 * routes/Registrations
 * Creates a new registrant record using the input parameter data;
 * and also triggers the side effects of a new record, e.g. sending confirmation email.
 * https://rock-the-vote.github.io/Voter-Registration-Tool-API-Docs/#registrations
 *  /api/v3/registrations.json
 */
namespace JonahWhale\RockTheVote;

require_once(dirname(__FILE__)."/../clients/RockTheVote.php");


class Registrations extends RockTheVote {

    protected $baseUrl = 'https://vr.rockthevote.com/api/v3/';
    var $stageBaseUrl = 'https://staging.rocky.rockthevote.com/api/v3/';
    /**
     * Required fields
     */
    var $lang = 'en';
    var $partner_id;
    /**
     * Required. ‘mm­dd­yyyy hh:mm:ss’ (for statistics)
     */

    var $created_at;
    /**
     *
     */
    var $updated_at;

    /**
     * Required. ‘mm-dd-yyyy’
     */
    var $date_of_birth;

    var $email_address;

    var $home_address;

    var $home_city;

    var $name_title;

    /**
     * Required. Series of alnum, length and format state specific
     */
    var $id_number;

    /**
     * Required, state id number, I think driver's liscense
     */
    var $state_id_number;

    /**
     *
     */
    var $first_registration;

    /**
     * Required. ‘zzzzz’ ( 5 digit)
     */
    var $home_zip_code;
    var $collect_email_address;
    var $source_tracking_id;
    var $partner_tracking_id;

    /**
     * Boolean
     */
    var $send_confirmation_reminder_emails;

    var $response = array();
    public function __construct($partner_id,$lang='en') {
        $this->partner_id = (int)$partner_id;
    }

    public function setIdNumber($id) {
        $this->id_number = trim($id);
        return $this->id_number;
    }
    /**
     * Required. ‘mm-dd-yyyy’
     */
    public function setDateOfBirth($date) {
        if(!$date) {
            return false;
        }
        $this->date_of_birth = date('m-d-Y',strtotime($date));
        return $this->date_of_birth;
    }

    public function setEmailAddress($email) {
        $this->email_address = $email;
        return $this->email_address;
    }

    public function setHomeAddress($address) {
        $this->home_address = $address;
        return $this->home_address;
    }

    public function setHomeCity($city) {
        $this->home_city = strip_tags(trim($city));
        return $this->home_city;
    }

    public function setHomeStateId($state) {
        $this->home_state_id = strip_tags(trim($state));
        return $this->home_state_id;
    }

    /**
     *  This maps to id_number, api quirk
     */

    public function setStateIdNumber($state_id_number) {

        $this->state_id_number = strip_tags(trim($state_id_number));
        $this->id_number = $this->state_id_number;
        return $this->state_id_number;
    }

    public function setHomeZipCode($zip) {
        $this->home_zip_code = strip_tags(trim($zip));
        return $this->home_zip_code;
    }

    public function setNameSuffix($v) {
        $this->name_suffix = strip_tags(trim($v));
        return $this->name_suffix;
    }

    public function setLastName($last_name) {
        $this->last_name = strip_tags(trim($last_name));
        return $this->last_name;
    }

    public function setFirstName($first_name) {
        $this->first_name = strip_tags(trim($first_name));
        return $this->first_name;
    }

    /**
     * Required
     * Required. Must be one of “Mr.”, “Mrs.”, “Miss”, “Ms.”, “Sr.”, “Sra.”, “Srta.”
     */

    public function setNameTitle($name_title) {
        $this->name_title = strip_tags(trim($name_title));
        return $this->name_title;
    }

    /**
     * @returns url of state registration
     */

    public function checkState($state){

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->baseUrl."/gregistrationstates.json",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_POSTFIELDS => "",
          CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache"
          ),
        ));

        $json = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($json);
        // dumpMe($response);
        foreach($response->states AS $s) {
            if($s->name==$state) {
                return $s->url;
            }
        }
        return false;
        // return($response);
    }

    /**
    * Damn my love of camel
    */

    public function setField($f,$v) {
        $debugMe = false;
        switch($f) {
            case"id_number":
            case"state_id_number":
                $this->setStateIdNumber($v);
                break;
            case"home_zip_code":
                $this->setHomeZipCode($v);
                break;
            case"home_state_id":
                $this->setHomeStateId($v);
                break;
            case"home_city":
                $this->setHomeCity($v);
                break;
            case"home_address":
                $this->setHomeAddress($v);
                break;
            case"email_address":
                $this->setEmailAddress($v);
                break;
            case"date_of_birth":
                $this->setDateOfBirth($v);
                break;
            case"name_title":
                $this->setNameTitle($v);
                break;
            case"first_name":
                $this->setFirstName($v);
                break;
            case"last_name":
                $this->setLastName($v);
                break;
            case"name_suffix":
                $this->setNameSuffix($v);
                break;
            default:
                if($debugMe) {
                    dump($f);
                    dump($v);
                }

        }
    }

    public function showCheckbox($field,$label) {

    }
    /**
     * Put the data
     */

    public function putOld() {
        $debug = false;
        // set post fields
        $post = [
            'registration[partner_id]'=>$this->partner_id,
            'registration[lang]' => $this->lang,
            'registration[date_of_birth]' => $this->date_of_birth,
            'registration[email_address]' => $this->email_address,
            'registration[home_address]' => $this->home_address,
            'registration[home_city]' => $this->home_city,
            'registration[home_state_id]' => $this->home_state_id,
            'registration[home_zip_code]' => $this->home_zip_code,
            'registration[last_name]' => $this->last_name,
            'registration[name_title]' => $this->name_title,
            'registration[id_number]' => $this->id_number,
            'registration[us_citizen]' => 1
        ];
        if($debug) {
            print_r($post);
        }
        $c = curl_init($this->baseUrl);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c, CURLOPT_POSTFIELDS, $post);

        $response = curl_exec($c);
        curl_close($c);
        $r = json_decode($response);
        $this->response = $r;
        return $this->response;
    }

    public function put() {
        $debugMe = false;
        // set post fields
        $post = [
            'registration[lang]' => 'en',
            'registration[partner_id]' => $this->partner_id,
            'registration[source_tracking_id]' => 'propeller',
            'registration[partner_tracking_id]' => 'propeller',
            'registration[first_registration]' => $this->first_registration,
            'registration[name_title]' => $this->name_title,
            'registration[first_name]' => $this->first_name,
            'registration[middle_name]' => $this->middle_name,
            'registration[last_name]' => $this->last_name,
            'registration[name_suffix]' => $this->name_suffix,
            'registration[date_of_birth]' => $this->date_of_birth,
            'registration[home_address]' => $this->home_address,
            'registration[home_unit]' => $this->home_unit,
            'registration[home_city]' => $this->home_city,
            'registration[home_state_id]' => $this->home_state_id,
            'registration[home_zip_code]' => $this->home_zip_code,
            'registration[has_mailing_address]' => $this->has_mailing_address,
            'registration[mailing_address]' => $_POST['mailAddress'],
            'registration[mailing_unit]' => $_POST['mailUnit'],
            'registration[mailing_city]' => $_POST['mailCity'],
            'registration[mailing_state_id]' => $_POST['mailState'],
            'registration[mailing_zip_code]' => $_POST['mailZip'],
            'registration[change_of_address]' => $_POST['hasPrevAddress'],
            'registration[prev_address]' => $_POST['prevAddress'],
            'registration[prev_unit]' => $_POST['prevUnit'],
            'registration[prev_city]' => $_POST['prevCity'],
            'registration[prev_state_id]' => $_POST['prevState'],
            'registration[prev_zip_code]' => $_POST['prevZip'],
            'registration[change_of_name]' => $_POST['hasChangedName'],
            'registration[prev_name_title]' => $_POST['prevTitle'],
            'registration[prev_first_name]' => $_POST['prevFirstName'],
            'registration[prev_middle_name]' => $_POST['prevMiddleName'],
            'registration[prev_last_name]' => $_POST['prevLastName'],
            'registration[prev_name_suffix]' => $_POST['prevSuffix'],
            'registration[us_citizen]' =>  1,
            'registration[has_state_license]' =>  $_POST['hasLicense'],
            'registration[is_eighteen_or_older]' =>  $this->is_eighteen_or_older,
            'registration[email_address]' => $this->email_address,
            'registration[phone]' => $_POST['phone'],
            'registration[party]' => $_POST['party'],
            'registration[race]' => $this->race,
            'registration[id_number]' => $this->id_number,
            'registration[opt_in_email]' => '0',
            'registration[opt_in_sms]' => '0',
            'registration[opt_in_volunteer]' => '0'
        ];
        if($debugMe) dump($post);
        $c = curl_init($this->baseUrl.'registrations.json');
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c, CURLOPT_POSTFIELDS, $post);

        $json = curl_exec($c);
        $response = json_decode($json);
        curl_close($c);
        if($debugMe) dump($response);

        return $response;
    }
}
