<?php
/**
 * @test
 * @version 1.0 Jonah B 2019
 */

class ZipToState {
    var $key = false;
    public function __construct($key=false) {
        if($key) {
            $this->key = $key;
        } else {
            $this->key = GOOGLE_GEO_API;
        }

    }
    /**
     *
     */
    public function findState($zip) {
        $debug = false;
        $zip = (int) $zip;
        $zinfo_json = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$zip.',USA&key='.$this->key);
        if($debug) {
            dump($zinfo_json);
        }
        $r = json_decode($zinfo_json);
        // dump($r->results[0]);
        if(is_object($r->results[0])) {
            foreach($r->results[0]->address_components as $c) {
                if($c->types[0] == 'administrative_area_level_1') {
                    $state = $c->short_name;
                }
            }
        } else {
            return false;
        }
        return $state;
    }
    /**
     *
     */
    public function findCity($zip) {
        $debug = false;
        $zip = (int) $zip;
        $zinfo_json = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$zip.',USA&key='.$this->key);
        if($debug) {
            // dump($zinfo_json);
        }
        $r = json_decode($zinfo_json);
        if($debug) {
            dump($r);
        }
        // dump($r->results[0]);
        if(is_object($r->results[0])) {
            $a = $this->parseAddress($r->results[0]);
        } else {
            return false;
        }
        return $a['city'];
    }

    function parseAddress($in) {
        $out = [];
        if($in->formatted_address) {
            $ps = explode(",",$in->formatted_address);
            $out['city'] = $ps[0];
        } else {
            return false;
        }
        return $out;
    }

}
