<?php
/**
 * PHP wrapper for Rock The Vote API
 *
 * Base class that all the other route classes extend from to access API client.
 * A Guzzle client is created with the provided API keys
 * and extended classes access the client via `$this->client()`
 *
 * @package RockTheVote
 * @since 0.0.1
 */
namespace JonahWhale\RockTheVote;

// require_once '../../../vendor/autoload.php';
use GuzzleHttp\Client;
/**
 * A client to access the Metrc API
 */
class RockTheVote
{
    var $partner_id;
    protected $apiUserKey;
    protected $baseUrl;
    protected $version;
    protected $client;
    /**
     * Create a new Client
     *
     * @param string $partnerId
     * @param string $apiUserKey
     */
    public function __construct($partner_id, $apiUserKey, $environment = 'production')
    {
        $this->stateAbbreviation = $stateAbbreviation;
        $this->version = $version;
        $this->apiVendorKey = $apiVendorKey;
        $this->apiUserKey = $apiUserKey;
        /**
         * Set the base URL according to production or sandbox parameter
         * If the user doesn't set one, we assume it's production
         */
        switch($environment)
        {
            case 'production':
                $this->production();
                break;
            case 'sandbox':
                $this->sandbox();
                break;
            default:
                $this->production();
                break;
        }
        /**
         * Create a new Guzzle client with constructed vars
         * Separated in case we need to reset client
         * (like switching from Sandbox to Production)
         */
        $this->newClient();
    }
    /**
     * Create a new Guzzle client using class' variables
     *
     * @return void
     */
    protected function newClient()
    {
        $this->client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $this->baseUrl,
            'auth' => [$this->apiVendorKey, $this->apiUserKey],
        ]);
    }
    /**
     * Changes the base URL to the METRC API sandbox URL
     * and creates a new client
     *
     * @return void
     */
    public function sandbox()
    {
        $this->baseUrl = "";
        $this->newClient();
    }
    /**
     * Changes the base URL to the METRC API production URL
     *
     * @return void
     */
    public function production()
    {
        $this->baseUrl = "";
        $this->newClient();
    }

    public function embed() {
        ?><iframe src=”https://register.rockthevote.com/?partner=<?= (int) $this->partnerId ?>?>&source=ovrpage” width=”100%” height=”1200″ marginheight=”0″ frameborder=”0″></iframe>
        <?php
    }

    /*
    * registrations
    *
    */
    public function registrations() {

    }

}
